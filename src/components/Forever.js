import React, {Component} from 'react';
import PropTypes from 'prop-types';

class Forever extends Component {
    static defaultProps = {};

    static propTypes = {};

    state = {};

    render() {
        return (
            <div className={"forever"}>
                <div className={"container"}>
                    <div className={"row"}>
                        <div className={"col-sm-4 text-left"}>
                            <img src={require('./../images/forevercard.png')} style={{width: '94%'}} alt=""/>
                        </div>
                        <div className={"col-sm-8"}>
                            <div className={"forever-content"}>
                                <h3>Toyota Forever</h3>
                                <p>Toyota sahipleri, Toyota Forever Kart sayesinde Toyota Plazalarda yaptıkları servis harcamalarında Toyota Puan kazanıyorlar. Üstelik Toyota Puan, kullandıkça artıyor, daha fazla indirim ve avantaj kazandırıyor.</p>
                                <button className={"btn btn-forever"}>Toyota Forever</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Forever;
