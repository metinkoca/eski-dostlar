import React, {Component} from 'react';
import PropTypes from 'prop-types';
import store from './../store/store';
import {updateUI} from "../store/actions/ui-actions";
import connect from "react-redux/es/connect/connect";

class AliceCarouselItem extends Component {
    static defaultProps = {};

    static propTypes = {};

    state = {
        isOpen: false
    };


    render() {
        return (
            <div onDragStart={this.props.onDragStart}
                 className={"model-item"}>
                {this.props.ui.isAdvantageMenuContentSelected && this.props.ui.isAdvantageMenuContentOpen !== this.props.dkey ?
                    <div className={"overlay-for-item"}></div> : null}
                <div onClick={() => {
                    this.setState({isOpen: !this.state.isOpen});
                    store.dispatch(updateUI({
                        isAdvantageMenuContentOpen: this.props.dkey,
                        isAdvantageMenuContentSelected: !this.state.isOpen,
                        engineDetails: this.props.engineDetails
                    }));
                }}>
                    <img
                        src={this.props.img} style={{width: '100%'}}
                        alt=""/>
                    <span className={"car-title"}>{this.props.title}</span>
                    <span className={"car-year"}>{this.props.year}</span>
                </div>
                {this.state.isOpen && this.props.ui.isAdvantageMenuContentOpen === this.props.dkey ?
                    <div className={"data"}>
                        <button onClick={() => {
                            this.setState({isOpen: !this.state.isOpen});
                            store.dispatch(updateUI({
                                isAdvantageMenuContentOpen: this.props.dkey,
                                isAdvantageMenuContentSelected: !this.state.isOpen,
                                engineDetails: this.props.engineDetails
                            }));
                        }} className={"close-model"}>Kapat
                        </button>
                        {this.props.children}
                    </div> : null}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        ui: state.ui,
        user: state.user
    };
};
export default connect(mapStateToProps)(AliceCarouselItem);
