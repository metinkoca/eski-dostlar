import React, {Component} from 'react';
import PropTypes from 'prop-types';

class SplitBanner extends Component {
    static defaultProps = {};

    static propTypes = {};

    state = {};

    render() {
        return (
            <div className={"split-banner"}>
                <div className={"container"}>
                    <div className={"row"}>
                        <div className={"col-sm-6"}></div>
                        <div className={"col-sm-6"}>Orijinal Toyota yedek parçaları, çok daha sağlam ve kaliteli olduğu
                            için aracınızı arızadan uzak tutar ve onu uzun süre keyifle kullanmanıza yardımcı olur.
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SplitBanner;
