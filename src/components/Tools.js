import React, {Component} from 'react';
import PropTypes from 'prop-types';
import FreeCheckUp from "./tools/FreeCheckUp";
import AdvantageMenus from "./tools/AdvantageMenus";
import OrginalSpare from "./tools/OrginalSpare";
import ToyotaForever from "./tools/ToyotaForever";

class Tools extends Component {
    static defaultProps = {};

    static propTypes = {};

    state = {};

    render() {
        return (
            <div>
                <FreeCheckUp/>
                <AdvantageMenus/>
                <OrginalSpare/>
                <ToyotaForever/>
                <div className={"clearfix"}></div>
            </div>
        );
    }
}

export default Tools;
