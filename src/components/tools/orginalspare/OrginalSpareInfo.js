import React, {Component} from 'react';
import PropTypes from 'prop-types';
import connect from "react-redux/es/connect/connect";

class OrginalSpareInfo extends Component {
    static defaultProps = {};

    static propTypes = {
        image: PropTypes.object,
        title: PropTypes.string,
        real: PropTypes.string,
        code: PropTypes.string
    };

    state = {};

    render() {
        return (
            <div className={"orginal-spare-info"}>
                <div className={"orginal-spare-info-box"}>
                    <div className={"orginal-spare-image-box"}>
                        {this.props.image ? <img src={this.props.image} alt=""/> : null}
                    </div>
                    <div className={"orginal-spare-info-content"}>
                        <span className={"orginal-spare-info-title"}>{this.props.title}</span>
                        {this.props.real ? <span className={"money"}>{this.props.real}</span> : null}
                        <div className={"clearfix"}></div>
                        <div className={"info-text"}><img src={require('./../../../images/warning.svg')} alt=""/>Tavsiye
                            Edilen Satış Fiyatı
                        </div>
                        <button className={"orginal-spare-code-button"}>
                            {this.props.code}
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        ui: state.ui,
        user: state.user
    };
};
export default connect(mapStateToProps)(OrginalSpareInfo);
