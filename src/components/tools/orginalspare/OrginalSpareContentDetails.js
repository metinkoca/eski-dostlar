import React, {Component} from 'react';
import PropTypes from 'prop-types';
import OrginalSpareInfo from "./OrginalSpareInfo";
import connect from "react-redux/es/connect/connect";
import {isMobile} from "react-device-detect";

class OrginalSpareContentDetails extends Component {
    static defaultProps = {};

    static propTypes = {};

    state = {
        orginalSpares: []
    };

    componentDidMount() {
        this.setState({
            orginalSpares: [
                {
                    image: require('./../../../images/brakes.png'),
                    title: "Ön Fren Balatası",
                    real: "202,74 TL",
                    code: "04465YZZAB"
                },
                {
                    image: require('./../../../images/brakes.png'),
                    title: "Ön Fren Balatası",
                    real: "202,74 TL",
                    code: "04465YZZAB"
                },
                {
                    image: require('./../../../images/brakes.png'),
                    title: "Ön Fren Balatası",
                    real: "202,74 TL",
                    code: "04465YZZAB"
                },
                {
                    image: require('./../../../images/brakes.png'),
                    title: "Ön Fren Balatası",
                    real: "202,74 TL",
                    code: "04465YZZAB"
                },
                {
                    image: require('./../../../images/brakes.png'),
                    title: "Ön Fren Balatası",
                    real: "202,74 TL",
                    code: "04465YZZAB"
                },
                {
                    image: require('./../../../images/brakes.png'),
                    title: "Ön Fren Balatası",
                    real: "202,74 TL",
                    code: "04465YZZAB"
                },
                {
                    image: require('./../../../images/brakes.png'),
                    title: "Ön Fren Balatası",
                    real: "202,74 TL",
                    code: "04465YZZAB"
                },
                {
                    image: require('./../../../images/brakes.png'),
                    title: "Ön Fren Balatası",
                    real: "202,74 TL",
                    code: "04465YZZAB"
                },
                {
                    image: require('./../../../images/brakes.png'),
                    title: "Ön Fren Balatası",
                    real: "202,74 TL",
                    code: "04465YZZAB"
                },
                {
                    image: require('./../../../images/brakes.png'),
                    title: "Ön Fren Balatası",
                    real: "202,74 TL",
                    code: "04465YZZAB"
                },
                {
                    image: require('./../../../images/brakes.png'),
                    title: "Ön Fren Balatası",
                    real: "202,74 TL",
                    code: "04465YZZAB"
                },
                {
                    image: require('./../../../images/brakes.png'),
                    title: "Ön Fren Balatası",
                    real: "202,74 TL",
                    code: "04465YZZAB"
                }
            ]
        });
    }

    render() {

        return (
            <div className={"advantage-menu-content-details"}>
                {this.props.ui.selectedModel && this.props.ui.activePage === "orginal-spare" && !isMobile ?
                    <div className={"advantage-menu-content-heading"}>
                        <div className={"container"}>
                            <div className={"pull-left"}>
                                <h3>Aracınıza Özel Cazip Fiyatlı Orijinal Yedek Parçalar</h3>
                            </div>
                            <div className={"pull-right text-right"}>
                                <b>Seçili araç :</b> Corolla 1993-1999, {this.props.ui.selectedModel}
                            </div>
                            <div className={"clearfix"}></div>
                        </div>
                    </div> : null}
                <div className={"container menu-info-list"}>
                    <div className={"row"}>
                        {this.state.orginalSpares.map(a => <div className={"col-sm-2 col-4"}>
                            <OrginalSpareInfo
                                image={a.image}
                                title={a.title}
                                real={a.real}
                                code={a.code}
                            />
                        </div>)}
                    </div>
                </div>
                <div className={"advantage-menu-content-desc"}>
                    <div className={"container"}>
                        Yukarıda belirtilen seçili araç modelleri; 1993-2007 arasındaki Corolla kasa ve 2006 ve
                        öncesindeki Yaris kasa araçları kapsamaktadır. Toyota Türkiye Pazarlama ve Satış A.Ş., tüm
                        fiyatları ve menü içeriklerini önceden haber vermeksizin değiştirme hakkını saklı
                        tutar.Avantajlı menüler ve cazip fiyatlı orijinal yedek parçalarda Toyota Forever indirimi
                        geçerli değildir.
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        ui: state.ui,
        user: state.user
    };
};
export default connect(mapStateToProps)(OrginalSpareContentDetails);
