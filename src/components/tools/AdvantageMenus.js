import React, {Component} from 'react';
import PropTypes from 'prop-types';
import connect from "react-redux/es/connect/connect";
import store from "../../store/store";
import {updateUI} from "../../store/actions/ui-actions";

class AdvantageMenus extends Component {
    static defaultProps = {};

    static propTypes = {};

    state = {
        isPassive: false
    };

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.ui.activePage === "advantage-menus") {
            this.setState({isPassive: false});
        } else if (!nextProps.ui.activePage) {
            this.setState({isPassive: false});
        } else {
            this.setState({isPassive: true});
        }
    }

    openPage = () => {
        if (this.props.ui.activePage === "advantage-menus") {
            store.dispatch(updateUI({activePage: null}));
        } else {
            store.dispatch(updateUI({activePage: 'advantage-menus'}));
        }
    };

    render() {
        return (
            <div className={this.state.isPassive ? "advantage-menus tool passive" : "advantage-menus tool"}>
                <img src={require("./../../images/alternate-m.jpg")} className={"tool-m-image"} alt=""/>
                <div className={"tool-container"}>
                    <b>Avantajlı Menüler</b>
                    <p>
                        1993-2007 arasındaki Corolla kasa ve 2006 ve öncesindeki Yaris kasa araçları avantajlı menüler.
                    </p>
                    <button className={"btn btn-advantage-menus"}
                            onClick={this.openPage}>Avantajlı
                        Menüler
                    </button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        ui: state.ui,
        user: state.user
    };
};
export default connect(mapStateToProps)(AdvantageMenus);
