import React, {Component} from 'react';
import PropTypes from 'prop-types';
import store from "../../store/store";
import {updateUI} from "../../store/actions/ui-actions";
import connect from "react-redux/es/connect/connect";

class OrginalSpare extends Component {
    static defaultProps = {};

    static propTypes = {};

    state = {
        isPassive: false
    };

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.ui.activePage === "orginal-spare") {
            this.setState({isPassive: false});
        } else if (!nextProps.ui.activePage) {
            this.setState({isPassive: false});
        } else {
            this.setState({isPassive: true});
        }
    }

    openPage = () => {
        if (this.props.ui.activePage === "orginal-spare") {
            store.dispatch(updateUI({activePage: null}));
        } else {
            store.dispatch(updateUI({activePage: 'orginal-spare'}));
        }
    };

    render() {
        return (
            <div className={this.state.isPassive ? "orginal-spare tool passive" : "orginal-spare tool"}>
                <img src={require("./../../images/cazip.png")} className={"tool-m-image"} alt=""/>
                <div className={"tool-container"}>
                    <b>Cazip Fiyatlı Orijinal Yedek Parçalar</b>
                    <p>
                        Aracınızın modeline uygun parça numarasına
                        ait tüm yedek parçalar.
                    </p>
                    <button className={"btn btn-spare-tool"} onClick={this.openPage}>Yedek Parçalar
                    </button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        ui: state.ui,
        user: state.user
    };
};
export default connect(mapStateToProps)(OrginalSpare);
