import React, {Component} from 'react';
import PropTypes from 'prop-types';
import connect from "react-redux/es/connect/connect";
import store from "../../store/store";
import {updateUI} from "../../store/actions/ui-actions";

class FreeCheckUp extends Component {
    static defaultProps = {};

    static propTypes = {};

    state = {
        isPassive: false
    };

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.ui.activePage === "free-check-up") {
            this.setState({isPassive: false});
        } else if (!nextProps.ui.activePage) {
            this.setState({isPassive: false});
        } else {
            this.setState({isPassive: true});
        }
    }

    openPage = () => {
        if (this.props.ui.activePage === "free-check-up") {
            store.dispatch(updateUI({activePage: null}));
        } else {
            store.dispatch(updateUI({activePage: 'free-check-up'}));
        }
    };

    render() {
        return (
            <div className={this.state.isPassive ? "free-check-up tool passive" : "free-check-up tool"}>
                <img src={require("./../../images/freecheckup.jpg")} className={"tool-m-image"} alt=""/>
                <div className={"tool-container"}>
                    <b>Ücretsiz Check-Up</b>
                    <p>
                        2007 ve öncesi Toyota’larda ücretsiz<br className={"hide-mobile"}/>check-up ve bir çok avantaj sunuyor.
                    </p>
                    <button className={"btn btn-check-up"}
                            onClick={this.openPage}>Ücretsiz Check-Up
                    </button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        ui: state.ui,
        user: state.user
    };
};
export default connect(mapStateToProps)(FreeCheckUp);
