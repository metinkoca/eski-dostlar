import React, {Component} from 'react';
import PropTypes from 'prop-types';
import connect from "react-redux/es/connect/connect";
import AliceCarousel from 'react-alice-carousel'
import AliceCarouselItem from "../../AliceCarouselItem";
import store from "./../../../store/store";
import {updateUI} from "../../../store/actions/ui-actions";
import {isMobile} from 'react-device-detect';

class AdvantageMenuContent extends Component {
    static defaultProps = {};

    static propTypes = {};

    state = {
        isActive: false,
        model: '',
        selectedModel: '',
        currentIndex: 0,
        activeSelection: false,
        models: [{
            title: 'Corolla',
            year: '1993 - 1999',
            engineDetails: [
                {
                    title: '1.6 Benzin, Karbüratörlü',
                    value: '1.6 Benzin, Karbüratörlü'
                },
                {
                    title: '1.6 Benzin, Enjeksiyonlu',
                    value: '1.6 Benzin, Enjeksiyonlu'
                },
                {
                    title: '1.3 Benzin, Karbüratörlü',
                    value: '1.3 Benzin, Karbüratörlü'
                }
            ]
        }, {
            title: 'Corolla',
            year: '1993 - 1999',
            engineDetails: [
                {
                    title: '1.6 Benzin, Karbüratörlü',
                    value: '1.6 Benzin, Karbüratörlü'
                },
                {
                    title: '1.6 Benzin, Enjeksiyonlu',
                    value: '1.6 Benzin, Enjeksiyonlu'
                },
                {
                    title: '1.3 Benzin, Karbüratörlü',
                    value: '1.3 Benzin, Karbüratörlü'
                }
            ]
        }, {
            title: 'Corolla',
            year: '1993 - 1999',
            engineDetails: [
                {
                    title: '1.6 Benzin, Karbüratörlü',
                    value: '1.6 Benzin, Karbüratörlü'
                },
                {
                    title: '1.6 Benzin, Enjeksiyonlu',
                    value: '1.6 Benzin, Enjeksiyonlu'
                },
                {
                    title: '1.3 Benzin, Karbüratörlü',
                    value: '1.3 Benzin, Karbüratörlü'
                }
            ]
        }, {
            title: 'Corolla',
            year: '1993 - 1999',
            engineDetails: [
                {
                    title: '1.6 Benzin, Karbüratörlü',
                    value: '1.6 Benzin, Karbüratörlü'
                },
                {
                    title: '1.6 Benzin, Enjeksiyonlu',
                    value: '1.6 Benzin, Enjeksiyonlu'
                },
                {
                    title: '1.3 Benzin, Karbüratörlü',
                    value: '1.3 Benzin, Karbüratörlü'
                }
            ]
        }, {
            title: 'Corolla',
            year: '1993 - 1999',
            engineDetails: [
                {
                    title: '1.6 Benzin, Karbüratörlü',
                    value: '1.6 Benzin, Karbüratörlü'
                },
                {
                    title: '1.6 Benzin, Enjeksiyonlu',
                    value: '1.6 Benzin, Enjeksiyonlu'
                },
                {
                    title: '1.3 Benzin, Karbüratörlü',
                    value: '1.3 Benzin, Karbüratörlü'
                }
            ]
        }, {
            title: 'Corolla',
            year: '1993 - 1999',
            engineDetails: [
                {
                    title: '1.6 Benzin, Karbüratörlü',
                    value: '1.6 Benzin, Karbüratörlü'
                },
                {
                    title: '1.6 Benzin, Enjeksiyonlu',
                    value: '1.6 Benzin, Enjeksiyonlu'
                },
                {
                    title: '1.3 Benzin, Karbüratörlü',
                    value: '1.3 Benzin, Karbüratörlü'
                }
            ]
        }, {
            title: 'Corolla',
            year: '1993 - 1999',
            engineDetails: [
                {
                    title: '1.6 Benzin, Karbüratörlü',
                    value: '1.6 Benzin, Karbüratörlü'
                },
                {
                    title: '1.6 Benzin, Enjeksiyonlu',
                    value: '1.6 Benzin, Enjeksiyonlu'
                },
                {
                    title: '1.3 Benzin, Karbüratörlü',
                    value: '1.3 Benzin, Karbüratörlü'
                }
            ]
        }, {
            title: 'Corolla',
            year: '1993 - 1999',
            engineDetails: [
                {
                    title: '1.6 Benzin, Karbüratörlü',
                    value: '1.6 Benzin, Karbüratörlü'
                },
                {
                    title: '1.6 Benzin, Enjeksiyonlu',
                    value: '1.6 Benzin, Enjeksiyonlu'
                },
                {
                    title: '1.3 Benzin, Karbüratörlü',
                    value: '1.3 Benzin, Karbüratörlü'
                }
            ]
        }]
    };

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.ui.activePage === "advantage-menus") {
            this.setState({isActive: true});
        } else if (!nextProps.ui.activePage) {
            this.setState({isActive: false});
        } else {
            this.setState({isActive: false});
        }
    }

    slideTo = (i) => this.setState({currentIndex: i});

    onSlideChanged = (e) => this.setState({currentIndex: e.item})

    slideNext = () => this.setState({currentIndex: this.state.currentIndex + 1});

    slidePrev = () => this.setState({currentIndex: this.state.currentIndex - 1});


    render() {
        const handleOnDragStart = (e) => e.preventDefault();
        if (this.state.isActive) {
            return (
                <div className={"advantage-menu-content"}>
                    <div className={"text-center"}>
                        <h3>Aradığınız Modeli seçin</h3>
                    </div>
                    {!this.state.selectedModel ? <div className={"model-container"}>
                        <div className={this.state.model === "corolla" ? "model active" : "model"}
                             onMouseEnter={() => this.setState({model: "corolla"})}
                             onMouseLeave={() => this.setState({model: ""})}
                             onClick={() => this.setState({selectedModel: 'corolla'})}>
                            {isMobile ? <img
                                src={this.state.model === "corolla" ? require("./../../../images/corolla-white-m.svg") : require("./../../../images/corolla-black-m.svg")}
                                alt=""/> : <img
                                src={this.state.model === "corolla" ? require("./../../../images/Corolla.svg") : require("./../../../images/Corolla-black.svg")}
                                alt=""/>}
                        </div>
                        <div className={this.state.model === "yaris" ? "model active" : "model"}
                             onMouseEnter={() => this.setState({model: "yaris"})}
                             onMouseLeave={() => this.setState({model: ""})}
                             onClick={() => this.setState({selectedModel: 'yaris'})}>
                            <img
                                src={this.state.model === "yaris" ? require("./../../../images/yaris.svg") : require("./../../../images/yaris-black.svg")}
                                alt="" style={{width: '140px'}}/>
                        </div>
                        <div className={this.state.model === "auris" ? "model active" : "model"}
                             onMouseEnter={() => this.setState({model: "auris"})}
                             onMouseLeave={() => this.setState({model: ""})}
                             onClick={() => this.setState({selectedModel: 'auris'})}>

                            {isMobile ? <img
                                src={this.state.model === "auris" ? require("./../../../images/auris.svg") : require("./../../../images/auris-black.svg")}
                                alt="" style={{height: '30px'}}/> : <img
                                src={this.state.model === "auris" ? require("./../../../images/auris.svg") : require("./../../../images/auris-black.svg")}
                                alt=""/>}
                        </div>
                        <div className={"clearfix"}></div>
                    </div> : !isMobile ? <div className={"model-container"}>
                        <button className={"btn btn-back"} onClick={() => this.setState({selectedModel: false})}>Geri
                        </button>
                        <div className={"container"}>
                            <div className={"row"}>
                                <div className={"col-sm-2"}>
                                    <img
                                        src={require("./../../../images/Corolla-black.svg")} style={{width: '100%'}}
                                        alt=""/>
                                </div>
                                <div className={"col-sm-1"}></div>
                                <div className={"col-sm-9"}>
                                    <AliceCarousel mouseTrackingEnabled
                                                   onSlideChanged={this.onSlideChanged}
                                                   dotsDisabled={true}
                                                   buttonsDisabled={false}
                                                   slideToIndex={this.state.currentIndex}
                                                   responsive={{
                                                       0: {
                                                           items: 3,
                                                       },
                                                       1024: {
                                                           items: 6
                                                       }
                                                   }}>
                                        {this.state.models.map((a, index) => <div>
                                            <AliceCarouselItem onDragStart={handleOnDragStart} title={a.title}
                                                               dkey={index}
                                                               slideTo={this.slideTo}
                                                               year={a.year}
                                                               img={require("./../../../images/xA.jpg")}>
                                                <span className={"data-title"}>Motor Tipini Seçiniz</span>
                                                {a.engineDetails.map((b, index) => <button
                                                    className={this.state.selectedModel === b.value ? "data-button active" : "data-button"}
                                                    onClick={() => {
                                                        this.setState({selectedModel: b.value});
                                                        store.dispatch(updateUI({selectedModel: b.value}));
                                                    }}>{b.title}</button>)}
                                            </AliceCarouselItem>
                                        </div>)}
                                    </AliceCarousel>


                                </div>
                            </div>
                        </div>
                    </div> : <div>
                        <div className={"model-mobile-container"}>
                            <div className={this.state.selectedModel === "auris" ? "model active" : "model"}
                                 onMouseEnter={() => this.setState({model: "auris"})}
                                 onMouseLeave={() => this.setState({model: ""})}
                                 onClick={() => this.setState({selectedModel: 'auris'})}>
                                <img
                                    src={this.state.selectedModel === "auris" && !isMobile ? require("./../../../images/auris.svg") : require("./../../../images/auris-black.svg")}
                                    alt=""/>
                            </div>
                            <div className={this.state.selectedModel === "corolla" ? "model active" : "model"}
                                 onMouseEnter={() => this.setState({model: "corolla"})}
                                 onMouseLeave={() => this.setState({model: ""})}
                                 onClick={() => this.setState({selectedModel: 'corolla'})}>
                                <img style={{
                                    maxWidth: '150%',
                                    marginLeft: '-15px',
                                    width: '140%',
                                }}
                                     src={this.state.model === "corolla" ? require("./../../../images/corolla-black-m.svg") : require("./../../../images/corolla-black-m.svg")}
                                     alt=""/>
                            </div>
                            <div className={this.state.selectedModel === "yaris" ? "model active" : "model"}
                                 onMouseEnter={() => this.setState({model: "yaris"})}
                                 onMouseLeave={() => this.setState({model: ""})}
                                 onClick={() => this.setState({selectedModel: 'yaris'})}>
                                <img
                                    src={this.state.selectedModel === "yaris" && !isMobile ? require("./../../../images/yaris.svg") : require("./../../../images/yaris-black.svg")}
                                    alt="" style={{width: '60px'}}/>
                            </div>
                            <div className={"clearfix"}></div>
                        </div>
                        <div className={"model-container"}>
                            <div className={"container"}>
                                <div className={"row"}>
                                    {this.state.models.map((a, index) => <div className={"col-4"}>
                                        <AliceCarouselItem onDragStart={handleOnDragStart} title={a.title}
                                                           dkey={index}
                                                           slideTo={this.slideTo}
                                                           year={a.year}
                                                           engineDetails={a.engineDetails}
                                                           img={require("./../../../images/xA.jpg")}>
                                            <span className={"data-title"}>Motor Tipini Seçiniz</span>
                                            {a.engineDetails.map((b, index) => <button
                                                className={this.state.selectedModel === b.value ? "data-button active" : "data-button"}
                                                onClick={() => {
                                                    this.setState({selectedModel: b.value});
                                                    store.dispatch(updateUI({selectedModel: b.value}));
                                                }}>{b.title}</button>)}
                                        </AliceCarouselItem>
                                    </div>)}

                                    <div className={"clearfix"}></div>
                                </div>
                                <div className={"clearfix"}></div>
                            </div>
                            <div className={"clearfix"}></div>
                        </div>
                        <div className={"clearfix"}></div>
                        {this.props.ui.engineDetails ? <div className={"choose-engine"}>
                            <div
                                className={"seelected-engine"}
                                onClick={() => this.setState({activeSelection: true})}>{this.state.asdmodel ? this.state.asdmodel : "Motor Tipini Seçiniz"}</div>
                            {this.state.activeSelection ? <div className={"show-engine-options"}>
                                {this.props.ui.engineDetails ? this.props.ui.engineDetails.map(b => <button
                                    onClick={() => {
                                        this.setState({
                                            selectedModel: b.value,
                                            asdmodel: b.value,
                                            activeSelection: false
                                        });
                                        store.dispatch(updateUI({selectedModel: b.value}));
                                    }}>{b.title}</button>) : null}
                            </div> : null}
                        </div> : null}
                    </div>}

                </div>
            );
        } else {
            return null;
        }
    }
}

const mapStateToProps = (state) => {
    return {
        ui: state.ui,
        user: state.user
    };
};
export default connect(mapStateToProps)(AdvantageMenuContent);
