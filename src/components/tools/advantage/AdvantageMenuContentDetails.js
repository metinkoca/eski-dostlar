import React, {Component} from 'react';
import PropTypes from 'prop-types';
import AdvantageMenuInfo from "./AdvantageMenuInfo";
import connect from "react-redux/es/connect/connect";
import {isMobile} from "react-device-detect";

class AdvantageMenuContentDetails extends Component {
    static defaultProps = {};

    static propTypes = {};

    state = {
        advantageMenus: [
            {
                title: "Motor yağı değişim menüsü",
                content: "Orijinal Toyota Motor Yağı<br/>\n" +
                    "                        Yağ Filtresi<br/>\n" +
                    "                        Karter Tapa Contası",
                del: "240 TL",
                real: "182 TL"
            },
            {
                title: "Motor yağı değişim menüsü",
                content: "Orijinal Toyota Motor Yağı<br/>\n" +
                    "                        Yağ Filtresi<br/>\n" +
                    "                        Karter Tapa Contası",
                del: "240 TL",
                real: "182 TL"
            },
            {
                title: "Motor yağı değişim menüsü",
                content: "Orijinal Toyota Motor Yağı<br/>\n" +
                    "                        Yağ Filtresi<br/>\n" +
                    "                        Karter Tapa Contası",
                del: "240 TL",
                real: "182 TL"
            },
            {
                title: "Motor yağı değişim menüsü",
                content: "Orijinal Toyota Motor Yağı<br/>\n" +
                    "                        Yağ Filtresi<br/>\n" +
                    "                        Karter Tapa Contası",
                del: "240 TL",
                real: "182 TL"
            },
            {
                title: "Motor yağı değişim menüsü",
                content: "Orijinal Toyota Motor Yağı<br/>\n" +
                    "                        Yağ Filtresi<br/>\n" +
                    "                        Karter Tapa Contası",
                del: "240 TL",
                real: "182 TL"
            },
            {
                title: "Motor yağı değişim menüsü",
                content: "Orijinal Toyota Motor Yağı<br/>\n" +
                    "                        Yağ Filtresi<br/>\n" +
                    "                        Karter Tapa Contası",
                del: "240 TL",
                real: "182 TL"
            },
            {
                title: "Motor yağı değişim menüsü",
                content: "Orijinal Toyota Motor Yağı<br/>\n" +
                    "                        Yağ Filtresi<br/>\n" +
                    "                        Karter Tapa Contası",
                del: "240 TL",
                real: "182 TL"
            },
            {
                title: "Motor yağı değişim menüsü",
                content: "Orijinal Toyota Motor Yağı<br/>\n" +
                    "                        Yağ Filtresi<br/>\n" +
                    "                        Karter Tapa Contası",
                del: "240 TL",
                real: "182 TL"
            }
        ]
    };


    render() {
        return (
            <div className={"advantage-menu-content-details"}>
                {this.props.ui.selectedModel && this.props.ui.activePage === "advantage-menus" ?
                    <div className={"advantage-menu-content-heading"}>
                        <div className={"container"}>
                            <div className={"pull-left"}>
                                <h3>Aracınıza ait avantajlı menüler</h3>
                            </div>
                            <div className={"pull-right text-right"}>
                                <b>Seçili araç :</b> Corolla 1993-1999, {this.props.ui.selectedModel}
                            </div>
                            <div className={"clearfix"}></div>
                        </div>
                    </div> : null}
                <div className={"container menu-info-list"}>
                    <div className={"row"}>
                        {this.state.advantageMenus.map(a => <div className={"col-sm-3 col-6"}>
                            <AdvantageMenuInfo
                                title={a.title}
                                content={a.content}
                                del={a.del}
                                real={a.real}
                            />
                        </div>)}
                    </div>
                </div>
                <div className={"advantage-menu-content-desc"}>
                    <div className={"container"}>
                        Avantajlı menülerde belirtilen fiyatlar, KDV ve işçilik dahil tavsiye edilen satış
                        fiyatlarıdır.
                        Aracınıza uygun menülerin tespiti için lütfen Toyota Yetkili servislerine başvurunuz.
                    </div>
                </div>
            </div>
        );

    }
}

const mapStateToProps = (state) => {
    return {
        ui: state.ui,
        user: state.user
    };
};
export default connect(mapStateToProps)(AdvantageMenuContentDetails);
