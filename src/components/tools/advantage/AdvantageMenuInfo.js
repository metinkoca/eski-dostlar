import React, {Component} from 'react';
import PropTypes from 'prop-types';
import connect from "react-redux/es/connect/connect";

class AdvantageMenuInfo extends Component {
    static defaultProps = {};

    static propTypes = {
        title: PropTypes.string,
        content: PropTypes.string,
        del: PropTypes.string,
        real: PropTypes.string
    };

    state = {};

    render() {
        return (
            <div className={"advantage-menu-info"}>
                <span className={"advantage-menu-info-title"}>{this.props.title}</span>
                <div className={"advantege-menu-info-box"}>
                    <p dangerouslySetInnerHTML={{__html: this.props.content}}></p>
                    <div className={"row"}>
                        <div className={"col-sm-9"}>
                            {this.props.del ? <span className={"del"}>{this.props.del}</span> : null}
                            {this.props.real ? <span className={"money"}>{this.props.real}</span> : null}
                            <div className={"clearfix"}></div>
                            <div className={"info-text"}><img src={require('./../../../images/warning.svg')} alt=""/>İşçilik
                                Dahil, İndirimli Fiyat
                            </div>
                        </div>
                        <div className={"col-sm-3"}>
                            <div className={"icon-for-original"}>
                                <img src={require('./../../../images/shock-struts.png')} alt=""/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={"offered-selling-price"}>
                    <button className={"offered-selling-price-button"}>
                        Tavsiye Edilen Satış Fiyatı
                    </button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        ui: state.ui,
        user: state.user
    };
};
export default connect(mapStateToProps)(AdvantageMenuInfo);
