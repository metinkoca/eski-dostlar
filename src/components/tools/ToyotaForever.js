import React, {Component} from 'react';
import PropTypes from 'prop-types';
import store from "../../store/store";
import {updateUI} from "../../store/actions/ui-actions";
import connect from "react-redux/es/connect/connect";

class ToyotaForever extends Component {
    static defaultProps = {};

    static propTypes = {};

    state = {
        isPassive: false
    };

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.ui.activePage === "orginal-spare") {
            this.setState({isPassive: false});
        } else if (!nextProps.ui.activePage) {
            this.setState({isPassive: false});
        } else {
            this.setState({isPassive: true});
        }
    }

    openPage = () => {
        if (this.props.ui.activePage === "orginal-spare") {
            store.dispatch(updateUI({activePage: null}));
        } else {
            store.dispatch(updateUI({activePage: 'orginal-spare'}));
        }
    };

    render() {
        return (
            <div className={this.state.isPassive ? "orginal-spare tool passive toyota-forever-mobil" : "orginal-spare tool toyota-forever-mobil"}>
                <img src={require("./../../images/forever-mobil.png")} className={"tool-m-image"} alt=""/>
                <div className={"tool-container"}>
                    <b>Toyota Forever</b>
                    <p>
                        Toyota sahipleri , Toyota Forever Kart sayesinde ,Toyota Puan kazanıyorlar.
                    </p>
                    <button className={"btn btn-spare-tool"}>Toyota Forever</button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        ui: state.ui,
        user: state.user
    };
};
export default connect(mapStateToProps)(ToyotaForever);
