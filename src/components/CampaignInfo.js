import React, {Component} from 'react';
import PropTypes from 'prop-types';

class CampaignInfo extends Component {
    static defaultProps = {};

    static propTypes = {};

    state = {};

    render() {
        return (
            <div className={"campaign-info"}>
                <div className={"container"}>
                    <a href="#" className={"btn btn-find-seller"}>Bayimizi Bulun</a>
                    <a href="#" className={"btn btn-e-broschure"}>E-Broşür</a>
                    <a href="#" className={"btn btn-new-toyota"}>Yeni Toyota İstiyorum</a>
                    <div className={"clearfix"}></div>
                    <p>2019 Kampanyaları burada! Modellerimizin güncel fırsatlarına göz atın.</p>
                </div>
            </div>
        );
    }
}

export default CampaignInfo;
