import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {isMobile} from 'react-device-detect';
import connect from "react-redux/es/connect/connect";

class Slider extends Component {
    static defaultProps = {};

    static propTypes = {};

    state = {};

    render() {
        return (
            <div className={"slider-old"}>
                <div className={"container"}>
                    <div className={"row"}>
                        {this.props.ui.activePage === "advantage-menus" && isMobile ? <div className={"col-sm-6"}>
                            <h3>Avantajlı Menüler</h3>
                            <p>1993-2007 arasındaki Corolla kasa, Yaris kasa<br/>araçları avantajlı menüler.</p>
                        </div> : null}
                        {this.props.ui.activePage === "orginal-spare" && isMobile ? <div className={"col-sm-6"}>
                            <h3>Eski Dostlar Programı</h3>
                            <h4>Eski Dostlara Yüz Yılın Fırsatı Toyota'da!</h4>
                            <p>Hiç kimse Toyota'nızı bizden daha iyi tanımıyor. Aracınıza mükemmel ş̧ekilde uyan<br
                                className={"hide-mobile"}/>yedek parçalar için bize güvenin.</p>
                        </div> : null}
                        {!isMobile || (this.props.ui.activePage !== "orginal-spare" && this.props.ui.activePage !== "advantage-menus") ?
                            <div className={"col-sm-6"}>
                                <h3>Eski Dostlar Programı</h3>
                                <h4>Eski Dostlara Yüz Yılın Fırsatı Toyota'da!</h4>
                                <p>Hiç kimse Toyota'nızı bizden daha iyi tanımıyor. Aracınıza mükemmel ş̧ekilde
                                    uyan<br
                                        className={"hide-mobile"}/>yedek parçalar için bize güvenin.</p>
                            </div> : null}
                        <div className={"col-sm-6"}></div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        ui: state.ui,
        user: state.user
    };
};
export default connect(mapStateToProps)(Slider);
