import {createStore, combineReducers} from 'redux';
import user from "./reducers/userReducer";
import ui from "./reducers/uiReducer";

const rootReducer = combineReducers({
    user: user,
    ui: ui,
});

const store = createStore(rootReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
store.subscribe(() => {
})
export default store;
