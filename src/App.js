import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Slider from "./components/Slider";
import './App.css';
import Tools from "./components/Tools";
import Forever from "./components/Forever";
import CampaignInfo from "./components/CampaignInfo";
import connect from "react-redux/es/connect/connect";
import AdvantageMenuContent from "./components/tools/advantage/AdvantageMenuContent";
import "react-alice-carousel/lib/alice-carousel.css";
import AdvantageMenuContentDetails from "./components/tools/advantage/AdvantageMenuContentDetails";
import OrginalSpareContent from "./components/tools/orginalspare/OrginalSpareContent";
import OrginalSpareContentDetails from "./components/tools/orginalspare/OrginalSpareContentDetails";
import SplitBanner from "./components/SplitBanner";
import {isMobile} from 'react-device-detect';


class App extends Component {
    static defaultProps = {};

    static propTypes = {};

    state = {};

    componen

    render() {
        return (
            <div>
                <Slider/>
                {this.props.ui.activePage && isMobile ? null : <Tools/>}
                {!this.props.ui.selectedModel ? <AdvantageMenuContent/> : null}
                {this.props.ui.selectedModel && this.props.ui.activePage === "advantage-menus" ?
                    <AdvantageMenuContentDetails/> : null}
                {this.props.ui.selectedModel && this.props.ui.activePage === "advantage-menus" ? <SplitBanner/> : null}
                {this.props.ui.selectedModel && this.props.ui.activePage === "advantage-menus" ?
                    <OrginalSpareContentDetails/> : null}
                <OrginalSpareContent/>
                {this.props.ui.selectedModel && this.props.ui.activePage === "orginal-spare" ?
                    <OrginalSpareContentDetails/> : null}
                {this.props.ui.selectedModel && this.props.ui.activePage === "orginal-spare" ? <SplitBanner/> : null}
                {this.props.ui.selectedModel && this.props.ui.activePage === "orginal-spare" ?
                    <AdvantageMenuContentDetails/> : null}
                <Forever/>
                <CampaignInfo/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        ui: state.ui,
        user: state.user
    };
};
export default connect(mapStateToProps)(App);
